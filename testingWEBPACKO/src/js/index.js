import num from "./test";
console.log(`I imported ${num} from another module.`);

const somethingConst = "howdy partner";

console.log(`oh just an es6 test: ${somethingConst}`);

class Person {
  constructor(name) {
    this.name = name;
  }
  sayHi() {
    console.log("Hellaso");
  }
}

let jobin = new Person("jobani");
